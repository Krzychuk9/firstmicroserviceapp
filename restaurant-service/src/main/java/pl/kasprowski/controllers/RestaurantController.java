package pl.kasprowski.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.kasprowski.domain.Entity;
import pl.kasprowski.domain.Restaurant;
import pl.kasprowski.dto.RestaurantDto;
import pl.kasprowski.service.RestaurantService;

import java.util.Collection;

@RestController
@RequestMapping("/v1/restaurants")
public class RestaurantController {

    private RestaurantService restaurantService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Collection<Restaurant> findByName(@RequestParam String name) {
        String query = name.toLowerCase();
        try {
            return restaurantService.findByName(query);
        } catch (Exception e) {
            throw new RuntimeException("Not found");
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{restaurantId}")
    @ResponseStatus(HttpStatus.OK)
    public Entity findById(@PathVariable String restaurantId) {
        try {
            return restaurantService.findById(restaurantId);
        } catch (Exception e) {
            throw new RuntimeException("Not found");
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Restaurant createNewRestaurant(@RequestBody RestaurantDto restaurantDto) {
        Restaurant restaurant = new Restaurant();
        BeanUtils.copyProperties(restaurantDto, restaurant);
        try {
            restaurantService.add(restaurant);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restaurant;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleException() {
        return "Not found!";
    }
}
