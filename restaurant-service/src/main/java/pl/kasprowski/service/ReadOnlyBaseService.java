package pl.kasprowski.service;

import pl.kasprowski.repository.Repository;

/**
 * Created by Admin on 2017-10-08.
 */
public abstract class ReadOnlyBaseService<TE, T> {

    private Repository<TE, T> repository;

    public ReadOnlyBaseService(Repository<TE, T> repository) {
        this.repository = repository;
    }
}
