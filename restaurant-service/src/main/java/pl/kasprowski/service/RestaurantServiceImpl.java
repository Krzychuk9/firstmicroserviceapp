package pl.kasprowski.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.Entity;
import pl.kasprowski.domain.Restaurant;
import pl.kasprowski.repository.RestaurantRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

@Service
public class RestaurantServiceImpl extends BaseService<Restaurant, String> implements RestaurantService {

    private RestaurantRepository restaurantRepository;

    @Autowired
    public RestaurantServiceImpl(RestaurantRepository repository) {
        super(repository);
        this.restaurantRepository = repository;
    }

    @Override
    public void add(Restaurant restaurant) throws Exception {
        String restaurantName = restaurant.getName();
        if (restaurantRepository.containsName(restaurantName)) {
            throw new Exception(String.format("There is already a restaurant with the name - %s", restaurantName));
        }

        if (restaurantName == null || "".equals(restaurantName)) {
            throw new Exception("Restaurant name cannot be null or empty string.");
        }
        super.add(restaurant);
    }

    @Override
    public void update(Restaurant restaurant) throws Exception {
        restaurantRepository.update(restaurant);
    }

    @Override
    public void delete(String id) throws Exception {
        restaurantRepository.remove(id);
    }

    @Override
    public Entity findById(String restaurantId) throws Exception {
        return restaurantRepository.get(restaurantId);
    }

    @Override
    public Collection<Restaurant> findByName(String name) throws Exception {
        return restaurantRepository.findByName(name);
    }

    @Override
    public Collection<Restaurant> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Collection<Restaurant> getAll() {
        return super.getAll();
    }
}
