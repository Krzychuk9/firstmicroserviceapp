package pl.kasprowski.repository;

import pl.kasprowski.domain.Restaurant;

import java.util.Collection;

public interface RestaurantRepository extends Repository<Restaurant, String> {

    boolean containsName(String name);

    public Collection<Restaurant> findByName(String name) throws Exception;
}
