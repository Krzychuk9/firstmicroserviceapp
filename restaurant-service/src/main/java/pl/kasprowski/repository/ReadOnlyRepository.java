package pl.kasprowski.repository;

import pl.kasprowski.domain.Entity;

import java.util.Collection;

public interface ReadOnlyRepository<TE, T> {

    boolean contains(T id);

    Entity<T> get(T id);

    Collection<TE> getAll();
}
